## ToDo App [[View Live Version](https://hayat-todo-app.herokuapp.com/)]

A simple full stack containerized application.

### **Running with Docker**

```
docker-compose up --build
```

### **Running Locally**

```
1. cd client && npm i npm start
1. cd server && npm i npm start
```

### **Running Client Locally**

```
1. cd client
2. npm install
3. npm start
```

### **Running Server Locally**

```
1. cd server
2. npm install
3. npm start
```

### License

ToDo App is [MIT licensed](./LICENSE).
