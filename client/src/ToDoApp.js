import React from "react";
import List from "./components/List";
import Alert from "./components/Alert";
import "./ToDoApp.css";

function ToDoApp() {
  const [todo, setTodo] = React.useState("");
  const [todoList, setTodoList] = React.useState([]);
  const [alert, setAlert] = React.useState({ show: false, msg: "", type: "" });

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!todo) {
      showAlert(true, "danger", "Please enter value");
    } else {
      try {
        const payload = {
          method: "post",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ item: todo }),
        };
        const response = await fetch(
          "http://localhost:3001/api/todos",
          payload
        );
        if (response.ok) {
          const todoData = await response.json();
          setTodoList([...todoList, todoData]);
          setTodo("");
          showAlert(true, "success", "Item added to the list");
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  const showAlert = (show = false, type = "", msg = "") => {
    setAlert({ show, type, msg });
  };

  const init = React.useCallback(async () => {
    try {
      const response = await fetch("http://localhost:3001/api/todos");
      if (response.ok) {
        const todoData = await response.json();
        setTodoList(todoData);
      }
    } catch (error) {
      console.error(error);
    }
  }, [setTodoList]);

  React.useEffect(() => {
    init();
  }, [init]);

  return (
    <section className="section-center">
      <form className="todo-form" onSubmit={handleSubmit}>
        {alert.show && (
          <Alert {...alert} removeAlert={showAlert} todoList={todoList} />
        )}
        <h3>Todo List</h3>
        <div className="form-control">
          <input
            type="text"
            className="todo"
            value={todo}
            onChange={(e) => setTodo(e.target.value)}
          />
          <button type="submit" className="submit-btn">
            Submit!
          </button>
        </div>
      </form>
      {todoList.length > 0 && (
        <div className="todo-container">
          <List todoList={todoList} />
        </div>
      )}
    </section>
  );
}

export default ToDoApp;
