import { shallow } from "enzyme";
import Alert from "../components/Alert";

describe("Alert", () => {
  it("Renders Alert without crashing", () => {
    shallow(<Alert />);
  });

  it("Renders paragraph without crashing", () => {
    const mountedAlert = shallow(<Alert />);
    const p = mountedAlert.find("p");
    expect(p.length).toBe(1);
  });
});
