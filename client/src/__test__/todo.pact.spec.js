import path from "path";
import { API } from "../todoApi";
import { Pact } from "@pact-foundation/pact";
import { eachLike } from "@pact-foundation/pact/dsl/matchers";

const provider = new Pact({
  consumer: "ToDoApp",
  provider: "ToDosService",
  log: path.resolve(process.cwd(), "logs", "pact.log"),
  logLevel: "warn",
  dir: path.resolve(process.cwd(), "pacts"),
  spec: 2,
});

describe("API Pact test", () => {
  beforeAll(() => provider.setup());
  afterEach(() => provider.verify());
  afterAll(() => provider.finalize());

  describe("getting all todos", () => {
    test("todos exists", async () => {
      // set up Pact interactions
      await provider.addInteraction({
        state: "todos exist",
        uponReceiving: "get all todos",
        withRequest: {
          method: "GET",
          path: "/api/todos",
        },
        willRespondWith: {
          status: 200,

          body: eachLike({ _id: 1, item: "Test todo item" }),
        },
      });

      const api = new API(provider.mockService.baseUrl);

      // make request to Pact mock server
      const product = await api.getAllToDos();

      expect(product).toStrictEqual([{ _id: 1, item: "Test todo item" }]);
    });

    test("no todos exists", async () => {
      // set up Pact interactions
      await provider.addInteraction({
        state: "no todos exist",
        uponReceiving: "get all todos",
        withRequest: {
          method: "GET",
          path: "/api/todos",
        },
        willRespondWith: {
          status: 200,
          body: [],
        },
      });

      const api = new API(provider.mockService.baseUrl);

      // make request to Pact mock server
      const product = await api.getAllToDos();

      expect(product).toStrictEqual([]);
    });
  });
});
