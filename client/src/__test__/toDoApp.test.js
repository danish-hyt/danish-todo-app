import { shallow } from "enzyme";
import ToDoApp from "../ToDoApp";

describe("ToDoApp", () => {
  it("Renders ToDoApp without crashing", () => {
    shallow(<ToDoApp />);
  });

  it("Renders section without crashing", () => {
    const mountedToDoApp = shallow(<ToDoApp />);
    const section = mountedToDoApp.find("section");
    expect(section.length).toBe(1);
  });

  it("Renders form without crashing", () => {
    const mountedToDoApp = shallow(<ToDoApp />);
    const form = mountedToDoApp.find("form");
    expect(form.length).toBe(1);
  });

  it("Renders input without crashing", () => {
    const mountedToDoApp = shallow(<ToDoApp />);
    const input = mountedToDoApp.find("input");
    expect(input.length).toBe(1);
  });

  it("Renders button without crashing", () => {
    const mountedToDoApp = shallow(<ToDoApp />);
    const button = mountedToDoApp.find("button");
    expect(button.length).toBe(1);
  });
});
