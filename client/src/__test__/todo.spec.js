import nock from "nock";
import API from "../todoApi";

describe("API", () => {
  test("get all todos", async () => {
    const todos = [
      { _id: 1, item: "Test todo item" },
      { _id: 2, item: "Test todo item2" },
    ];
    nock(API.url)
      .get("/api/todos")
      .reply(200, todos, { "Access-Control-Allow-Origin": "*" });
    const toDos = await API.getAllToDos();
    expect(toDos).toEqual(todos);
  });
});
