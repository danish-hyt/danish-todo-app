import { shallow } from "enzyme";
import List from "../components/List";

const props = { todoList: [{ _id: 1, item: "Test todo item" }] };

describe("List", () => {
  it("Renders List without crashing", () => {
    shallow(<List {...props} />);
  });

  it("Renders div without crashing", () => {
    const mountedList = shallow(<List {...props} />);
    const div = mountedList.find("div");
    expect(div.length).toBe(1);
  });

  it("Renders article without crashing", () => {
    const mountedList = shallow(<List {...props} />);
    const article = mountedList.find("article");
    expect(article.length).toBe(1);
  });

  it("Renders paragraph without crashing", () => {
    const mountedApp = shallow(<List {...props} />);
    const p = mountedApp.find("p");
    expect(p.length).toBe(1);
  });
});
