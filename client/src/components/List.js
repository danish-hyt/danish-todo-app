import React from "react";

const List = ({ todoList }) => {
  return (
    <div className="todo-list">
      {todoList.map(({ _id, item }) => {
        return (
          <article className="todo-item" key={_id}>
            <p className="title">{item}</p>
          </article>
        );
      })}
    </div>
  );
};

export default List;
