const request = require("supertest");
const expect = require("chai").expect;
const app = require("../index");

describe("GET /api/todos", () => {
  it("Gets all toDos", () => {
    return request(app)
      .get("/api/todos")
      .then((res) => {
        const body = res.body;
        expect(body.length).to.equal(1);
      })
      .catch((err) => console.log(err));
  });
});
