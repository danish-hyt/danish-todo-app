const express = require("express");
const morgan = require("morgan");
const path = require("path");
const cors = require("cors");

const todoRoutes = require("./routes/api/todos");
const { connectDB } = require("./config/db");

const PORT = process.env.PORT || 3001;

const app = express();
app.use(express.static(path.join(__dirname, "../client/build")));

app.use(express.json({ extended: false }));
app.use(cors());
app.use(morgan("dev"));

app.use("/api/todos", todoRoutes);

app.get("/", (_, res) =>
    res.sendFile(path.join(__dirname, "../client/build/index.html"))
);

connectDB();

app.listen(PORT, () => console.log(`Server started on ${PORT}`));

module.exports = app;
