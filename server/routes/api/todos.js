const express = require("express");
const router = express.Router();
const ToDo = require("../../models/todo");

router.get("/", async (_, res) => {
  try {
    const toDos = await ToDo.find();
    res.status(200).json(toDos);
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error.");
  }
});

router.post("/", async (req, res) => {
  const { item } = req.body;
  try {
    let todo = await ToDo.findOne({ item });

    if (todo) {
      return res.status(400).send("ToDo item already exists");
    }
    // Creating ToDo
    const newToDo = new ToDo({ item });

    const toDo = await newToDo.save();

    res.json(toDo);
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error.");
  }
});

module.exports = router;
