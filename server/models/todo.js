const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ToDoSchema = new Schema({
  item: {
    type: String,
    unique: true,
  },
});

module.exports = ToDo = mongoose.model("todo", ToDoSchema);
